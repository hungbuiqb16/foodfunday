<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $regexPhone = config('const.regex_phone');
        return [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'phone' => "required|regex:$regexPhone",
            'no_of_persons' => 'required',
            'date_picker' => 'required|after:now',
            'time_picker' => 'required',
            'preferred_food' => 'required',
            'occasion' => 'required',
            'attach_file' => [
                'nullable',
                'mimes:jpeg,png',
                'mimetypes:image/jpeg,image/png',
                'max:3072'
            ],
        ];
    }
}
