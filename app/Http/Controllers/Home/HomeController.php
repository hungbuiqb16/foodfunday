<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Reservation;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\BookingRequest;
use Storage, Exception;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }


    public function reservation(BookingRequest $request)
    {
        try {
            DB::beginTransaction();

            $reservation = new Reservation;
            if($request->hasFile('attach_file')) {
                $fileName = $request->attach_file->getClientOriginalName();
                $filePath = config('const.path_project_attachment') . time() . '-' . $fileName;
                $path = Storage::disk('s3')->put($filePath, file_get_contents($request->attach_file));
                $reservation->attach_file = $filePath;
            }
            $reservation->name =  $request->name;
            $reservation->phone =  $request->phone;
            $reservation->email =  $request->email;
            $reservation->no_of_persons =  $request->no_of_persons;
            $reservation->date_picker =  $this->formatDateInput($request->date_picker);
            $reservation->time_picker =  $this->convert12To24($request->time_picker);
            $reservation->preferred_food =  $request->preferred_food;
            $reservation->occasion =  $request->occasion;
            $reservation->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
        return back();
    }

    private function formatDateInput($date)
    {
        $date = date_create($date);
        return date_format($date,"Y-m-d");
    }

    private function convert12To24($time12){
        $time24 = date("H:i", strtotime($time12));
        return $time24;
    }
}
