<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.pages.dashboards.index');
    }

    public function reservations()
    {

        return view('admin.pages.reservations.index');
    }
}
