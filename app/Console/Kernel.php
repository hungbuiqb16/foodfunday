<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->call(function () {
            DB::table('reservations')->insert(
                [
                    'name' => 'Akari',
                    'email' => 'akari@example.com',
                    'phone' => '0987654263',
                    'date_picker' => '2022-03-21',
                    'time_picker' => '12:21',
                    'no_of_persons' => 1,
                    'occasion' => 1,
                    'preferred_food' => 1,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
