@extends('admin.layouts.layout')
@push('styles')
    <!-- Table datatable css -->
    <link href="{{ asset('static/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('static/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('static/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('static/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
@endpush
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Buttons example</h4>
                <p class="sub-header">
                    The Buttons extension for DataTables provides a common set of options, API methods and styling to display buttons on a page that will interact with a DataTable. The core library provides the based framework upon which plug-ins can built.
                </p>

                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Salary</th>
                    </tr>
                    </thead>


                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                    </tr>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>63</td>
                        <td>2011/07/25</td>
                        <td>$170,750</td>
                    </tr>
                    <tr>
                        <td>Ashton Cox</td>
                        <td>Junior Technical Author</td>
                        <td>San Francisco</td>
                        <td>66</td>
                        <td>2009/01/12</td>
                        <td>$86,000</td>
                    </tr>
                    <tr>
                        <td>Cedric Kelly</td>
                        <td>Senior Javascript Developer</td>
                        <td>Edinburgh</td>
                        <td>22</td>
                        <td>2012/03/29</td>
                        <td>$433,060</td>
                    </tr>
                    <tr>
                        <td>Serge Baldwin</td>
                        <td>Data Coordinator</td>
                        <td>Singapore</td>
                        <td>64</td>
                        <td>2012/04/09</td>
                        <td>$138,575</td>
                    </tr>
                    <tr>
                        <td>Zenaida Frank</td>
                        <td>Software Engineer</td>
                        <td>New York</td>
                        <td>63</td>
                        <td>2010/01/04</td>
                        <td>$125,250</td>
                    </tr>
                    <tr>
                        <td>Jonas Alexander</td>
                        <td>Developer</td>
                        <td>San Francisco</td>
                        <td>30</td>
                        <td>2010/07/14</td>
                        <td>$86,500</td>
                    </tr>
                    <tr>
                        <td>Shad Decker</td>
                        <td>Regional Director</td>
                        <td>Edinburgh</td>
                        <td>51</td>
                        <td>2008/11/13</td>
                        <td>$183,000</td>
                    </tr>
                    <tr>
                        <td>Michael Bruce</td>
                        <td>Javascript Developer</td>
                        <td>Singapore</td>
                        <td>29</td>
                        <td>2011/06/27</td>
                        <td>$183,000</td>
                    </tr>
                    <tr>
                        <td>Donna Snider</td>
                        <td>Customer Support</td>
                        <td>New York</td>
                        <td>27</td>
                        <td>2011/01/25</td>
                        <td>$112,000</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@prepend('scripts')
    <!-- Datatable plugin js -->
    <script src="{{asset('static/libs/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('static/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <script src="{{asset('static/libs/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('static/libs/datatables/responsive.bootstrap4.min.js')}}"></script>

    <script src="{{asset('static/libs/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('static/libs/datatables/buttons.bootstrap4.min.js')}}"></script>

    <script src="{{asset('static/libs/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('static/libs/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('static/libs/pdfmake/vfs_fonts.js')}}"></script>

    <script src="{{asset('static/libs/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('static/libs/datatables/buttons.print.min.js')}}"></script>

    <script src="{{asset('static/libs/datatables/dataTables.keyTable.min.js')}}"></script>
    <script src="{{asset('static/libs/datatables/dataTables.select.min.js')}}"></script>

{{--    <!-- Datatables init -->--}}
{{--    <script src="{{asset('static/js/pages/datatables.init.js')}}"></script>--}}
@endprepend
