<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ 'Admin Dashboard ' .env('APP_NAME') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Responsive bootstrap 4 admin template" name="description">
    <meta content="Coderthemes" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('static/images/favicon_admin.ico')}}">
    <!-- App css -->
    <link href="{{asset('static/css/admin/bootstrap.min.css')}}" rel="stylesheet" type="text/css" id="bootstrap-stylesheet">
    <link href="{{asset('static/css/admin/icons.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('static/css/admin/app.min.css') }}" rel="stylesheet" type="text/css" id="app-stylesheet">
    @stack('styles')
</head>
<body class="left-side-menu-dark topbar-light">
<!-- Begin page -->
<div id="wrapper">
    <!-- Topbar Start -->
   @include('admin.elements.top_bar')
    <!-- end Topbar -->
    <!-- ========== Left Sidebar Start ========== -->
    @include('admin.elements.sidebar')
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <!-- start page title -->
                @include('admin.elements.breadcrumb')
                <!-- end page title -->
                @yield('content')
                <!-- end row -->
            </div> <!-- end container-fluid -->
        </div> <!-- end content -->
        <!-- Footer Start -->
        @include('admin.elements.footer')
        <!-- end Footer -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->
</div>
<!-- END wrapper -->
<!-- Vendor js -->
<script src="{{asset('static/js/admin/vendor.min.js')}}"></script>
@stack('scripts')
<!-- App js -->
<script src="{{asset('static/js/admin/app.min.js')}}"></script>
</body>
</html>
