<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="{{route('admin.dashboard.index')}}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> About Us </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('admin.reservations.layout.blade.php')}}">
                        <i class="mdi mdi-calendar-month"></i>
                        <span> Reservations </span>
                        <span class="badge badge-danger badge-pill float-right">Hot</span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);">
                        <i class="mdi mdi-google-pages"></i>
                        <span> Menu </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level mm-collapse" aria-expanded="false">
                        <li><a href="">Starter</a></li>
                        <li><a href="#">Main Disher</a></li>
                        <li><a href="#">Deserts</a></li>
                        <li><a href="#">Drinks</a></li>
                    </ul>
                </li>

                <li>
                    <a href="">
                        <i class="mdi mdi-calendar-month"></i>
                        <span> Teams </span>
                    </a>
                </li>

                <li>
                    <a href="">
                        <i class="mdi mdi-calendar-month"></i>
                        <span> Gallery </span>
                    </a>
                </li>

                <li>
                    <a href="">
                        <i class="mdi mdi-calendar-month"></i>
                        <span> Blogs </span>
                    </a>
                </li>

                <li>
                    <a href="">
                        <i class="mdi mdi-calendar-month"></i>
                        <span> Pricing </span>
                    </a>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
