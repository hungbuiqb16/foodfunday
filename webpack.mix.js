const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

    mix.copyDirectory('resources/assets/images', 'public/static/images');
    mix.copyDirectory('resources/assets/fonts/', 'public/static/fonts');

    mix.copyDirectory('resources/assets/js', 'public/static/js');

    mix.copyDirectory('resources/assets/css', 'public/static/css');

mix.copyDirectory('resources/assets/libs', 'public/static/libs');


