<?php
return [
    // config regex  phone
    'regex_phone' => '/^[0-9-]{10,13}$/i',
    // project attachments
    'path_project_attachment' => 'uploads/attachments/',
];
